package com.eclipsekingdom.hookshot;

import com.eclipsekingdom.hookshot.util.HookUtil;
import com.eclipsekingdom.hookshot.util.HookshotFactory;
import com.eclipsekingdom.hookshot.util.Stun;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.Collection;

public class HookListener implements Listener {

    public HookListener() {
        Plugin plugin = Hookshot.getPlugin();
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onPickup(PlayerPickupArrowEvent e) {
        if (HookAction.isHook(e.getArrow())) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onHit(ProjectileHitEvent e) {
        Projectile p = e.getEntity();
        if (HookAction.isHook(p)) {
            HookAction hookAction = HookAction.getActionFromHook(p);
            if (e.getHitEntity() != null) {
                hookAction.onHitEntity(e.getHitEntity());
            } else if (e.getHitBlock() != null) {
                hookAction.onHitBlock(e.getHitBlock(), e.getHitBlockFace());
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageByEntityEvent e) {
        if (HookAction.isHook(e.getDamager())) {
            HookAction hookAction = HookAction.getActionFromHook(e.getDamager());
            if (e.getEntity().getUniqueId().equals(hookAction.getPlayer().getUniqueId())) {
                hookAction.end();
                e.setCancelled(true);
                e.getDamager().remove();
            } else {
                e.setDamage(2);
            }
        }
    }

    @EventHandler
    public void onUse(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if (e.getHand() == EquipmentSlot.HAND && HookshotFactory.isHookshot(e.getItem())) {
            Action action = e.getAction();
            if (action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) {
                if (!isInteracting(player, e.getClickedBlock())) {
                    e.setCancelled(true);
                    if (!HookAction.isHookingPlayer(player)) {
                        new HookAction(player, e.getItem());
                    }
                }
            }
        }

    }

    @EventHandler
    public void onInteractAt(PlayerInteractAtEntityEvent e) {
        if (HookAction.isHookingPlayer(e.getPlayer())) e.setCancelled(true);
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractEntityEvent e) {
        if (HookAction.isHookingPlayer(e.getPlayer())) e.setCancelled(true);
    }

    private boolean isInteracting(Player player, Block block) {
        return block != null && HookUtil.isInteractable(block.getType()) && !player.isSneaking();
    }

    @EventHandler
    public void swapHand(PlayerSwapHandItemsEvent e) {
        Player player = e.getPlayer();
        if (HookAction.isHookingPlayer(player)) {
            if (!HookshotFactory.isHookshot(e.getMainHandItem())) {
                HookAction.getActionFromPlayer(player).cancelAction();
            }
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        Player player = e.getPlayer();
        if (HookAction.isHookingPlayer(player)) {
            HookAction.getActionFromPlayer(player).cancelAction();
            ItemStack itemStack = e.getItemDrop().getItemStack();
            ItemMeta meta = itemStack.getItemMeta();
            meta.setCustomModelData(300);
            itemStack.setItemMeta(meta);
        }
    }

    @EventHandler
    public void onToggle(PlayerItemHeldEvent e) {
        Player player = e.getPlayer();
        if (HookAction.isHookingPlayer(player)) {
            if (!HookshotFactory.isHookshot(player.getInventory().getItem(e.getNewSlot()))) {
                HookAction.getActionFromPlayer(player).cancelAction();
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (HookAction.isHookingPlayer(e.getWhoClicked())) {
            Player player = (Player) e.getWhoClicked();
            if (HookAction.isHookingPlayer(player)) {
                PlayerInventory inventory = player.getInventory();
                int heldSlot = inventory.getHeldItemSlot();
                if (e.getSlot() == heldSlot) {
                    endHookAction(player, e.getCurrentItem());
                } else {
                    if (e.getClick() == ClickType.NUMBER_KEY) {
                        if (e.getHotbarButton() == heldSlot) {
                            if (!HookshotFactory.isHookshot(inventory.getItem(e.getSlot()))) {
                                endHookAction(player, inventory.getItem(heldSlot));
                            }
                        }
                    }
                }
            }
        }
    }

    private void endHookAction(Player player, ItemStack itemStack) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.setCustomModelData(300);
        itemStack.setItemMeta(meta);
        HookAction.getActionFromPlayer(player).cancelAction();
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player player = e.getEntity();
        if (HookAction.isHookingPlayer(player)) {
            HookAction.getActionFromPlayer(player).cancelAction();
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        if (HookAction.isHookingPlayer(player)) {
            HookAction.getActionFromPlayer(player).cancelAction();
        }
    }

    @EventHandler
    public void onEnchant(PrepareItemEnchantEvent e) {
        if (HookshotFactory.isHookshot(e.getItem())) e.setCancelled(true);
    }

    @EventHandler
    public void onTarget(EntityTargetEvent e) {
        Entity entity = e.getEntity();
        if (Stun.isStunned(entity)) e.setCancelled(true);
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent e) {
        Entity entity = e.getEntity();
        if (Stun.isStunned(entity)) Stun.getStun(entity).end();
    }

    @EventHandler
    public void onTeleport(EntityTeleportEvent e) {
        Entity entity = e.getEntity();
        if (HookAction.isHookedEntity(entity)) {
            HookAction.getActionFromHookedEntity(entity).setState(HookAction.State.FAST_RETRACT);
        } else if (HookAction.isHookingPlayer(entity)) {
            HookAction.getActionFromPlayer((Player) entity).cancelAction();
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onExplodeBlock(BlockExplodeEvent e) {
        checkDestroyHook(e.blockList());
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onExplodeEntity(EntityExplodeEvent e) {
        checkDestroyHook(e.blockList());
    }

    private void checkDestroyHook(Collection<Block> blocks) {
        for (Block block : blocks) {
            Location location = block.getLocation();
            if (HookAction.isHookedBlockLoc(location)) {
                HookAction.getActionFromBlockLoc(location).cancelAction();
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBreakBlock(BlockBreakEvent e) {
        Location location = e.getBlock().getLocation();
        if (HookAction.isHookedBlockLoc(location)) {
            HookAction.getActionFromBlockLoc(location).cancelAction();
        }
    }

}
