package com.eclipsekingdom.hookshot.util;

import com.eclipsekingdom.hookshot.sys.ConsoleSender;
import com.eclipsekingdom.hookshot.sys.Language;
import com.eclipsekingdom.hookshot.version.VUtil;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class HookUtil {

    public static void setModel(ItemStack itemStack, int model) {
        ItemMeta meta = itemStack.getItemMeta();
        meta.setCustomModelData(model);
        itemStack.setItemMeta(meta);
    }

    public static void save(FileConfiguration config, File file) {
        try {
            config.save(file);
        } catch (Exception e) {
            ConsoleSender.sendMessage(Language.CONSOLE_FILE_ERROR.fromFile(file.getName()));
        }
    }

    public static boolean isHeavy(Entity entity) {
        if (VUtil.getBigEntities().contains(entity.getType())) {
            if (entity instanceof Ageable) {
                return ((Ageable) entity).isAdult();
            } else if (entity instanceof Slime) {
                return ((Slime) entity).getSize() > 3;
            } else if (entity instanceof Phantom) {
                return ((Phantom) entity).getSize() > 6;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    private static Set<Material> metalArmorMaterials = buildMetalArmorMaterials();

    private static Set<Material> buildMetalArmorMaterials() {

        Set<XMaterial> xMaterials = new HashSet<>();

        xMaterials.add(XMaterial.CHAINMAIL_CHESTPLATE);
        xMaterials.add(XMaterial.IRON_CHESTPLATE);
        xMaterials.add(XMaterial.GOLDEN_CHESTPLATE);
        xMaterials.add(XMaterial.DIAMOND_CHESTPLATE);
        xMaterials.add(XMaterial.NETHERITE_CHESTPLATE);
        xMaterials.add(XMaterial.IRON_HORSE_ARMOR);
        xMaterials.add(XMaterial.GOLDEN_HORSE_ARMOR);
        xMaterials.add(XMaterial.DIAMOND_HORSE_ARMOR);

        Set<Material> metalArmorMaterials = new HashSet<>();

        for (XMaterial xMaterial : xMaterials) {
            if (xMaterial.isSupported()) metalArmorMaterials.add(xMaterial.parseMaterial());
        }

        return metalArmorMaterials;
    }


    public static boolean hasProtection(Entity entity) {
        if (entity instanceof LivingEntity) {
            ItemStack itemStack = ((LivingEntity) entity).getEquipment().getChestplate();
            return (itemStack != null && metalArmorMaterials.contains(itemStack.getType()));
        } else if (entity.getType() == EntityType.UNKNOWN) {
            return true;
        } else {
            return false;
        }
    }

    public static int parseAmount(String string) {
        int amt;
        try {
            amt = Integer.parseInt(string);
            if (amt < 0) {
                amt = 0;
            }
            return amt;
        } catch (Exception e) {
            return 1;
        }
    }

    private static Set<Material> interactableMaterial = buildInteractableMaterials();

    private static Set<Material> buildInteractableMaterials() {
        Set<XMaterial> xMaterials = new HashSet<>();

        xMaterials.add(XMaterial.DISPENSER);
        xMaterials.add(XMaterial.NOTE_BLOCK);
        xMaterials.add(XMaterial.CHEST);
        xMaterials.add(XMaterial.CRAFTING_TABLE);
        xMaterials.add(XMaterial.FURNACE);
        xMaterials.add(XMaterial.OAK_TRAPDOOR);
        xMaterials.add(XMaterial.SPRUCE_TRAPDOOR);
        xMaterials.add(XMaterial.BIRCH_TRAPDOOR);
        xMaterials.add(XMaterial.JUNGLE_TRAPDOOR);
        xMaterials.add(XMaterial.ACACIA_TRAPDOOR);
        xMaterials.add(XMaterial.DARK_OAK_TRAPDOOR);
        xMaterials.add(XMaterial.WARPED_TRAPDOOR);
        xMaterials.add(XMaterial.CRIMSON_TRAPDOOR);
        xMaterials.add(XMaterial.OAK_DOOR);
        xMaterials.add(XMaterial.SPRUCE_DOOR);
        xMaterials.add(XMaterial.BIRCH_DOOR);
        xMaterials.add(XMaterial.JUNGLE_DOOR);
        xMaterials.add(XMaterial.ACACIA_DOOR);
        xMaterials.add(XMaterial.DARK_OAK_DOOR);
        xMaterials.add(XMaterial.WARPED_DOOR);
        xMaterials.add(XMaterial.CRIMSON_DOOR);
        xMaterials.add(XMaterial.OAK_FENCE_GATE);
        xMaterials.add(XMaterial.SPRUCE_FENCE_GATE);
        xMaterials.add(XMaterial.BIRCH_FENCE_GATE);
        xMaterials.add(XMaterial.JUNGLE_FENCE_GATE);
        xMaterials.add(XMaterial.ACACIA_FENCE_GATE);
        xMaterials.add(XMaterial.DARK_OAK_FENCE_GATE);
        xMaterials.add(XMaterial.WARPED_FENCE_GATE);
        xMaterials.add(XMaterial.CRIMSON_FENCE_GATE);
        xMaterials.add(XMaterial.ENCHANTING_TABLE);
        xMaterials.add(XMaterial.ENDER_CHEST);
        xMaterials.add(XMaterial.ANVIL);
        xMaterials.add(XMaterial.CHIPPED_ANVIL);
        xMaterials.add(XMaterial.DAMAGED_ANVIL);
        xMaterials.add(XMaterial.TRAPPED_CHEST);
        xMaterials.add(XMaterial.DAYLIGHT_DETECTOR);
        xMaterials.add(XMaterial.HOPPER);
        xMaterials.add(XMaterial.DROPPER);
        xMaterials.add(XMaterial.SHULKER_BOX);
        xMaterials.add(XMaterial.WHITE_SHULKER_BOX);
        xMaterials.add(XMaterial.ORANGE_SHULKER_BOX);
        xMaterials.add(XMaterial.MAGENTA_SHULKER_BOX);
        xMaterials.add(XMaterial.LIGHT_BLUE_SHULKER_BOX);
        xMaterials.add(XMaterial.YELLOW_SHULKER_BOX);
        xMaterials.add(XMaterial.LIME_SHULKER_BOX);
        xMaterials.add(XMaterial.PINK_SHULKER_BOX);
        xMaterials.add(XMaterial.GRAY_SHULKER_BOX);
        xMaterials.add(XMaterial.LIGHT_GRAY_SHULKER_BOX);
        xMaterials.add(XMaterial.CYAN_SHULKER_BOX);
        xMaterials.add(XMaterial.PURPLE_SHULKER_BOX);
        xMaterials.add(XMaterial.BLUE_SHULKER_BOX);
        xMaterials.add(XMaterial.BROWN_SHULKER_BOX);
        xMaterials.add(XMaterial.GREEN_SHULKER_BOX);
        xMaterials.add(XMaterial.RED_SHULKER_BOX);
        xMaterials.add(XMaterial.BLACK_SHULKER_BOX);
        xMaterials.add(XMaterial.REPEATER);
        xMaterials.add(XMaterial.COMPARATOR);
        xMaterials.add(XMaterial.WHITE_BED);
        xMaterials.add(XMaterial.ORANGE_BED);
        xMaterials.add(XMaterial.MAGENTA_BED);
        xMaterials.add(XMaterial.LIGHT_BLUE_BED);
        xMaterials.add(XMaterial.YELLOW_BED);
        xMaterials.add(XMaterial.LIME_BED);
        xMaterials.add(XMaterial.PINK_BED);
        xMaterials.add(XMaterial.GRAY_BED);
        xMaterials.add(XMaterial.LIGHT_GRAY_BED);
        xMaterials.add(XMaterial.CYAN_BED);
        xMaterials.add(XMaterial.PURPLE_BED);
        xMaterials.add(XMaterial.BLUE_BED);
        xMaterials.add(XMaterial.BROWN_BED);
        xMaterials.add(XMaterial.GREEN_BED);
        xMaterials.add(XMaterial.RED_BED);
        xMaterials.add(XMaterial.BLACK_BED);
        xMaterials.add(XMaterial.BREWING_STAND);
        xMaterials.add(XMaterial.LOOM);
        xMaterials.add(XMaterial.COMPOSTER);
        xMaterials.add(XMaterial.BARREL);
        xMaterials.add(XMaterial.SMOKER);
        xMaterials.add(XMaterial.BLAST_FURNACE);
        xMaterials.add(XMaterial.CARTOGRAPHY_TABLE);
        xMaterials.add(XMaterial.FLETCHING_TABLE);
        xMaterials.add(XMaterial.GRINDSTONE);
        xMaterials.add(XMaterial.LECTERN);
        xMaterials.add(XMaterial.SMITHING_TABLE);
        xMaterials.add(XMaterial.STONECUTTER);
        xMaterials.add(XMaterial.BELL);
        xMaterials.add(XMaterial.COMMAND_BLOCK);
        xMaterials.add(XMaterial.BEACON);
        xMaterials.add(XMaterial.REPEATING_COMMAND_BLOCK);

        Set<Material> woodMaterials = new HashSet<>();

        for (XMaterial xMaterial : xMaterials) {
            if (xMaterial.isSupported()) woodMaterials.add(xMaterial.parseMaterial());
        }

        return woodMaterials;
    }

    public static boolean isInteractable(Material material) {
        return interactableMaterial.contains(material);
    }


    private static Set<Material> grappleMaterials = buildGrappleMaterials();

    private static Set<Material> buildGrappleMaterials() {
        Set<XMaterial> xMaterials = new HashSet<>();

        xMaterials.add(XMaterial.OAK_PLANKS);
        xMaterials.add(XMaterial.SPRUCE_PLANKS);
        xMaterials.add(XMaterial.BIRCH_PLANKS);
        xMaterials.add(XMaterial.JUNGLE_PLANKS);
        xMaterials.add(XMaterial.ACACIA_PLANKS);
        xMaterials.add(XMaterial.DARK_OAK_PLANKS);
        xMaterials.add(XMaterial.WARPED_PLANKS);
        xMaterials.add(XMaterial.CRIMSON_PLANKS);

        xMaterials.add(XMaterial.OAK_LOG);
        xMaterials.add(XMaterial.SPRUCE_LOG);
        xMaterials.add(XMaterial.BIRCH_LOG);
        xMaterials.add(XMaterial.JUNGLE_LOG);
        xMaterials.add(XMaterial.ACACIA_LOG);
        xMaterials.add(XMaterial.DARK_OAK_LOG);
        xMaterials.add(XMaterial.WARPED_STEM);
        xMaterials.add(XMaterial.CRIMSON_STEM);
        xMaterials.add(XMaterial.PISTON_HEAD);

        xMaterials.add(XMaterial.STRIPPED_OAK_LOG);
        xMaterials.add(XMaterial.STRIPPED_SPRUCE_LOG);
        xMaterials.add(XMaterial.STRIPPED_BIRCH_LOG);
        xMaterials.add(XMaterial.STRIPPED_JUNGLE_LOG);
        xMaterials.add(XMaterial.STRIPPED_ACACIA_LOG);
        xMaterials.add(XMaterial.STRIPPED_DARK_OAK_LOG);
        xMaterials.add(XMaterial.STRIPPED_WARPED_STEM);
        xMaterials.add(XMaterial.STRIPPED_CRIMSON_STEM);

        xMaterials.add(XMaterial.STRIPPED_OAK_WOOD);
        xMaterials.add(XMaterial.STRIPPED_SPRUCE_WOOD);
        xMaterials.add(XMaterial.STRIPPED_BIRCH_WOOD);
        xMaterials.add(XMaterial.STRIPPED_JUNGLE_WOOD);
        xMaterials.add(XMaterial.STRIPPED_ACACIA_WOOD);
        xMaterials.add(XMaterial.STRIPPED_DARK_OAK_WOOD);
        xMaterials.add(XMaterial.STRIPPED_WARPED_HYPHAE);
        xMaterials.add(XMaterial.STRIPPED_CRIMSON_HYPHAE);

        xMaterials.add(XMaterial.OAK_WOOD);
        xMaterials.add(XMaterial.SPRUCE_WOOD);
        xMaterials.add(XMaterial.BIRCH_WOOD);
        xMaterials.add(XMaterial.JUNGLE_WOOD);
        xMaterials.add(XMaterial.ACACIA_WOOD);
        xMaterials.add(XMaterial.DARK_OAK_WOOD);
        xMaterials.add(XMaterial.WARPED_HYPHAE);
        xMaterials.add(XMaterial.CRIMSON_HYPHAE);

        xMaterials.add(XMaterial.NOTE_BLOCK);
        xMaterials.add(XMaterial.POWERED_RAIL);
        xMaterials.add(XMaterial.DETECTOR_RAIL);

        xMaterials.add(XMaterial.OAK_SLAB);
        xMaterials.add(XMaterial.SPRUCE_SLAB);
        xMaterials.add(XMaterial.BIRCH_SLAB);
        xMaterials.add(XMaterial.JUNGLE_SLAB);
        xMaterials.add(XMaterial.ACACIA_SLAB);
        xMaterials.add(XMaterial.DARK_OAK_SLAB);
        xMaterials.add(XMaterial.WARPED_SLAB);
        xMaterials.add(XMaterial.CRIMSON_SLAB);

        xMaterials.add(XMaterial.BOOKSHELF);

        xMaterials.add(XMaterial.OAK_STAIRS);
        xMaterials.add(XMaterial.SPRUCE_STAIRS);
        xMaterials.add(XMaterial.BIRCH_STAIRS);
        xMaterials.add(XMaterial.JUNGLE_STAIRS);
        xMaterials.add(XMaterial.ACACIA_STAIRS);
        xMaterials.add(XMaterial.DARK_OAK_STAIRS);
        xMaterials.add(XMaterial.WARPED_STAIRS);
        xMaterials.add(XMaterial.CRIMSON_STAIRS);

        xMaterials.add(XMaterial.LADDER);
        xMaterials.add(XMaterial.RAIL);

        xMaterials.add(XMaterial.OAK_PRESSURE_PLATE);
        xMaterials.add(XMaterial.SPRUCE_PRESSURE_PLATE);
        xMaterials.add(XMaterial.BIRCH_PRESSURE_PLATE);
        xMaterials.add(XMaterial.JUNGLE_PRESSURE_PLATE);
        xMaterials.add(XMaterial.ACACIA_PRESSURE_PLATE);
        xMaterials.add(XMaterial.DARK_OAK_PRESSURE_PLATE);
        xMaterials.add(XMaterial.WARPED_PRESSURE_PLATE);
        xMaterials.add(XMaterial.CRIMSON_PRESSURE_PLATE);

        xMaterials.add(XMaterial.OAK_FENCE);
        xMaterials.add(XMaterial.SPRUCE_FENCE);
        xMaterials.add(XMaterial.BIRCH_FENCE);
        xMaterials.add(XMaterial.JUNGLE_FENCE);
        xMaterials.add(XMaterial.ACACIA_FENCE);
        xMaterials.add(XMaterial.DARK_OAK_FENCE);
        xMaterials.add(XMaterial.WARPED_FENCE);
        xMaterials.add(XMaterial.CRIMSON_FENCE);

        xMaterials.add(XMaterial.OAK_FENCE_GATE);
        xMaterials.add(XMaterial.SPRUCE_FENCE_GATE);
        xMaterials.add(XMaterial.BIRCH_FENCE_GATE);
        xMaterials.add(XMaterial.JUNGLE_FENCE_GATE);
        xMaterials.add(XMaterial.ACACIA_FENCE_GATE);
        xMaterials.add(XMaterial.DARK_OAK_FENCE_GATE);
        xMaterials.add(XMaterial.WARPED_FENCE_GATE);
        xMaterials.add(XMaterial.CRIMSON_FENCE_GATE);

        xMaterials.add(XMaterial.OAK_TRAPDOOR);
        xMaterials.add(XMaterial.SPRUCE_TRAPDOOR);
        xMaterials.add(XMaterial.BIRCH_TRAPDOOR);
        xMaterials.add(XMaterial.JUNGLE_TRAPDOOR);
        xMaterials.add(XMaterial.ACACIA_TRAPDOOR);
        xMaterials.add(XMaterial.DARK_OAK_TRAPDOOR);
        xMaterials.add(XMaterial.WARPED_TRAPDOOR);
        xMaterials.add(XMaterial.CRIMSON_TRAPDOOR);

        xMaterials.add(XMaterial.CHEST);
        xMaterials.add(XMaterial.TRAPPED_CHEST);

        xMaterials.add(XMaterial.ACTIVATOR_RAIL);

        xMaterials.add(XMaterial.OAK_DOOR);
        xMaterials.add(XMaterial.SPRUCE_DOOR);
        xMaterials.add(XMaterial.BIRCH_DOOR);
        xMaterials.add(XMaterial.JUNGLE_DOOR);
        xMaterials.add(XMaterial.ACACIA_DOOR);
        xMaterials.add(XMaterial.DARK_OAK_DOOR);
        xMaterials.add(XMaterial.WARPED_DOOR);
        xMaterials.add(XMaterial.CRIMSON_DOOR);

        xMaterials.add(XMaterial.OAK_LEAVES);
        xMaterials.add(XMaterial.SPRUCE_LEAVES);
        xMaterials.add(XMaterial.BIRCH_LEAVES);
        xMaterials.add(XMaterial.JUNGLE_LEAVES);
        xMaterials.add(XMaterial.ACACIA_LEAVES);
        xMaterials.add(XMaterial.DARK_OAK_LEAVES);

        xMaterials.add(XMaterial.OAK_SIGN);
        xMaterials.add(XMaterial.SPRUCE_SIGN);
        xMaterials.add(XMaterial.BIRCH_SIGN);
        xMaterials.add(XMaterial.JUNGLE_SIGN);
        xMaterials.add(XMaterial.ACACIA_SIGN);
        xMaterials.add(XMaterial.DARK_OAK_SIGN);
        xMaterials.add(XMaterial.WARPED_SIGN);
        xMaterials.add(XMaterial.CRIMSON_SIGN);

        xMaterials.add(XMaterial.LOOM);
        xMaterials.add(XMaterial.COMPOSTER);
        xMaterials.add(XMaterial.BARREL);
        xMaterials.add(XMaterial.CARTOGRAPHY_TABLE);
        xMaterials.add(XMaterial.FLETCHING_TABLE);
        xMaterials.add(XMaterial.LECTERN);
        xMaterials.add(XMaterial.BEEHIVE);

        xMaterials.add(XMaterial.CRAFTING_TABLE);
        xMaterials.add(XMaterial.SCAFFOLDING);
        xMaterials.add(XMaterial.TARGET);

        xMaterials.add(XMaterial.SPONGE);
        xMaterials.add(XMaterial.WET_SPONGE);
        xMaterials.add(XMaterial.WHITE_WOOL);
        xMaterials.add(XMaterial.ORANGE_WOOL);
        xMaterials.add(XMaterial.MAGENTA_WOOL);
        xMaterials.add(XMaterial.LIGHT_BLUE_WOOL);
        xMaterials.add(XMaterial.YELLOW_WOOL);
        xMaterials.add(XMaterial.LIME_WOOL);
        xMaterials.add(XMaterial.PINK_WOOL);
        xMaterials.add(XMaterial.GRAY_WOOL);
        xMaterials.add(XMaterial.LIGHT_GRAY_WOOL);
        xMaterials.add(XMaterial.CYAN_WOOL);
        xMaterials.add(XMaterial.PURPLE_WOOL);
        xMaterials.add(XMaterial.BLUE_WOOL);
        xMaterials.add(XMaterial.BROWN_WOOL);
        xMaterials.add(XMaterial.GREEN_WOOL);
        xMaterials.add(XMaterial.RED_WOOL);
        xMaterials.add(XMaterial.BLACK_WOOL);
        xMaterials.add(XMaterial.CACTUS);
        xMaterials.add(XMaterial.PUMPKIN);
        xMaterials.add(XMaterial.CARVED_PUMPKIN);
        xMaterials.add(XMaterial.JACK_O_LANTERN);
        xMaterials.add(XMaterial.BROWN_MUSHROOM_BLOCK);
        xMaterials.add(XMaterial.BROWN_MUSHROOM_BLOCK);
        xMaterials.add(XMaterial.MUSHROOM_STEM);
        xMaterials.add(XMaterial.MELON);
        xMaterials.add(XMaterial.HAY_BLOCK);
        xMaterials.add(XMaterial.WHITE_CARPET);
        xMaterials.add(XMaterial.ORANGE_CARPET);
        xMaterials.add(XMaterial.MAGENTA_CARPET);
        xMaterials.add(XMaterial.LIGHT_BLUE_CARPET);
        xMaterials.add(XMaterial.YELLOW_CARPET);
        xMaterials.add(XMaterial.LIME_CARPET);
        xMaterials.add(XMaterial.PINK_CARPET);
        xMaterials.add(XMaterial.GRAY_CARPET);
        xMaterials.add(XMaterial.LIGHT_GRAY_CARPET);
        xMaterials.add(XMaterial.CYAN_CARPET);
        xMaterials.add(XMaterial.PURPLE_CARPET);
        xMaterials.add(XMaterial.BLUE_CARPET);
        xMaterials.add(XMaterial.BROWN_CARPET);
        xMaterials.add(XMaterial.GREEN_CARPET);
        xMaterials.add(XMaterial.RED_CARPET);
        xMaterials.add(XMaterial.BLACK_CARPET);
        xMaterials.add(XMaterial.NETHER_WART_BLOCK);
        xMaterials.add(XMaterial.WARPED_WART_BLOCK);
        xMaterials.add(XMaterial.BONE_BLOCK);
        xMaterials.add(XMaterial.WHITE_BED);
        xMaterials.add(XMaterial.ORANGE_BED);
        xMaterials.add(XMaterial.MAGENTA_BED);
        xMaterials.add(XMaterial.LIGHT_BLUE_BED);
        xMaterials.add(XMaterial.YELLOW_BED);
        xMaterials.add(XMaterial.LIME_BED);
        xMaterials.add(XMaterial.PINK_BED);
        xMaterials.add(XMaterial.GRAY_BED);
        xMaterials.add(XMaterial.LIGHT_GRAY_BED);
        xMaterials.add(XMaterial.CYAN_BED);
        xMaterials.add(XMaterial.PURPLE_BED);
        xMaterials.add(XMaterial.BLUE_BED);
        xMaterials.add(XMaterial.BROWN_BED);
        xMaterials.add(XMaterial.GREEN_BED);
        xMaterials.add(XMaterial.RED_BED);
        xMaterials.add(XMaterial.BLACK_BED);
        xMaterials.add(XMaterial.BEE_NEST);
        xMaterials.add(XMaterial.CAMPFIRE);
        xMaterials.add(XMaterial.SOUL_CAMPFIRE);
        xMaterials.add(XMaterial.SLIME_BLOCK);
        xMaterials.add(XMaterial.HONEYCOMB_BLOCK);
        xMaterials.add(XMaterial.CHORUS_FLOWER);
        xMaterials.add(XMaterial.CHORUS_PLANT);
        xMaterials.add(XMaterial.DRIED_KELP_BLOCK);

        Set<Material> woodMaterials = new HashSet<>();

        for (XMaterial xMaterial : xMaterials) {
            if (xMaterial.isSupported()) woodMaterials.add(xMaterial.parseMaterial());
        }

        return woodMaterials;
    }

    public static boolean isGrappleMaterial(Material material) {
        return grappleMaterials.contains(material);
    }

    private static Set<Material> clinkMaterial = buildClinkMaterials();

    private static Set<Material> buildClinkMaterials() {
        Set<XMaterial> xMaterials = new HashSet<>();

        xMaterials.add(XMaterial.STONE);
        xMaterials.add(XMaterial.GRANITE);
        xMaterials.add(XMaterial.POLISHED_GRANITE);
        xMaterials.add(XMaterial.DIORITE);
        xMaterials.add(XMaterial.POLISHED_DIORITE);
        xMaterials.add(XMaterial.ANDESITE);
        xMaterials.add(XMaterial.POLISHED_ANDESITE);
        xMaterials.add(XMaterial.COBBLESTONE);
        xMaterials.add(XMaterial.BEDROCK);
        xMaterials.add(XMaterial.GOLD_ORE);
        xMaterials.add(XMaterial.IRON_ORE);
        xMaterials.add(XMaterial.COAL_ORE);
        xMaterials.add(XMaterial.NETHER_GOLD_ORE);
        xMaterials.add(XMaterial.GLASS);
        xMaterials.add(XMaterial.LAPIS_ORE);
        xMaterials.add(XMaterial.LAPIS_BLOCK);
        xMaterials.add(XMaterial.DISPENSER);
        xMaterials.add(XMaterial.SANDSTONE);
        xMaterials.add(XMaterial.CHISELED_SANDSTONE);
        xMaterials.add(XMaterial.SMOOTH_SANDSTONE);
        xMaterials.add(XMaterial.STICKY_PISTON);
        xMaterials.add(XMaterial.PISTON);
        xMaterials.add(XMaterial.GOLD_BLOCK);
        xMaterials.add(XMaterial.IRON_BLOCK);
        xMaterials.add(XMaterial.STONE_SLAB);
        xMaterials.add(XMaterial.SMOOTH_STONE_SLAB);
        xMaterials.add(XMaterial.SANDSTONE_SLAB);
        xMaterials.add(XMaterial.SMOOTH_SANDSTONE_SLAB);
        xMaterials.add(XMaterial.COBBLESTONE_SLAB);
        xMaterials.add(XMaterial.BRICK_SLAB);
        xMaterials.add(XMaterial.STONE_BRICK_SLAB);
        xMaterials.add(XMaterial.NETHER_BRICK_SLAB);
        xMaterials.add(XMaterial.QUARTZ_SLAB);
        xMaterials.add(XMaterial.RED_SANDSTONE_SLAB);
        xMaterials.add(XMaterial.CUT_RED_SANDSTONE_SLAB);
        xMaterials.add(XMaterial.PURPUR_SLAB);
        xMaterials.add(XMaterial.PRISMARINE_SLAB);
        xMaterials.add(XMaterial.PRISMARINE_BRICK_SLAB);
        xMaterials.add(XMaterial.DARK_PRISMARINE_SLAB);
        xMaterials.add(XMaterial.SMOOTH_QUARTZ_SLAB);
        xMaterials.add(XMaterial.SMOOTH_RED_SANDSTONE);
        xMaterials.add(XMaterial.SMOOTH_SANDSTONE);
        xMaterials.add(XMaterial.SMOOTH_STONE);
        xMaterials.add(XMaterial.BRICKS);
        xMaterials.add(XMaterial.MOSSY_COBBLESTONE);
        xMaterials.add(XMaterial.OBSIDIAN);
        xMaterials.add(XMaterial.PURPUR_BLOCK);
        xMaterials.add(XMaterial.PURPUR_PILLAR);
        xMaterials.add(XMaterial.PURPUR_STAIRS);
        xMaterials.add(XMaterial.DIAMOND_ORE);
        xMaterials.add(XMaterial.DIAMOND_BLOCK);
        xMaterials.add(XMaterial.FURNACE);
        xMaterials.add(XMaterial.COBBLESTONE_STAIRS);
        xMaterials.add(XMaterial.STONE_PRESSURE_PLATE);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_PRESSURE_PLATE);
        xMaterials.add(XMaterial.REDSTONE_ORE);
        xMaterials.add(XMaterial.ICE);
        xMaterials.add(XMaterial.BASALT);
        xMaterials.add(XMaterial.POLISHED_BASALT);
        xMaterials.add(XMaterial.GLOWSTONE);
        xMaterials.add(XMaterial.INFESTED_STONE);
        xMaterials.add(XMaterial.INFESTED_COBBLESTONE);
        xMaterials.add(XMaterial.INFESTED_STONE_BRICKS);
        xMaterials.add(XMaterial.INFESTED_MOSSY_STONE_BRICKS);
        xMaterials.add(XMaterial.INFESTED_CRACKED_STONE_BRICKS);
        xMaterials.add(XMaterial.INFESTED_CHISELED_STONE_BRICKS);
        xMaterials.add(XMaterial.STONE_BRICKS);
        xMaterials.add(XMaterial.MOSSY_STONE_BRICKS);
        xMaterials.add(XMaterial.CRACKED_STONE_BRICKS);
        xMaterials.add(XMaterial.CHISELED_STONE_BRICKS);
        xMaterials.add(XMaterial.IRON_BARS);
        xMaterials.add(XMaterial.GLASS_PANE);
        xMaterials.add(XMaterial.BRICK_STAIRS);
        xMaterials.add(XMaterial.STONE_BRICK_STAIRS);
        xMaterials.add(XMaterial.NETHER_BRICK);
        xMaterials.add(XMaterial.CRACKED_NETHER_BRICKS);
        xMaterials.add(XMaterial.CHISELED_NETHER_BRICKS);
        xMaterials.add(XMaterial.NETHER_BRICK_FENCE);
        xMaterials.add(XMaterial.NETHER_BRICK_STAIRS);
        xMaterials.add(XMaterial.ENCHANTING_TABLE);
        xMaterials.add(XMaterial.END_PORTAL_FRAME);
        xMaterials.add(XMaterial.END_STONE);
        xMaterials.add(XMaterial.END_STONE_BRICKS);
        xMaterials.add(XMaterial.REDSTONE_LAMP);
        xMaterials.add(XMaterial.SANDSTONE_STAIRS);
        xMaterials.add(XMaterial.EMERALD_ORE);
        xMaterials.add(XMaterial.ENDER_CHEST);
        xMaterials.add(XMaterial.EMERALD_BLOCK);
        xMaterials.add(XMaterial.BEACON);
        xMaterials.add(XMaterial.COBBLESTONE_WALL);
        xMaterials.add(XMaterial.MOSSY_COBBLESTONE_WALL);
        xMaterials.add(XMaterial.BRICK_WALL);
        xMaterials.add(XMaterial.PRISMARINE_WALL);
        xMaterials.add(XMaterial.RED_SANDSTONE_WALL);
        xMaterials.add(XMaterial.MOSSY_STONE_BRICK_WALL);
        xMaterials.add(XMaterial.GRANITE_WALL);
        xMaterials.add(XMaterial.STONE_BRICK_WALL);
        xMaterials.add(XMaterial.NETHER_BRICK_WALL);
        xMaterials.add(XMaterial.ANDESITE_WALL);
        xMaterials.add(XMaterial.RED_NETHER_BRICK_WALL);
        xMaterials.add(XMaterial.SANDSTONE_WALL);
        xMaterials.add(XMaterial.END_STONE_BRICK_WALL);
        xMaterials.add(XMaterial.DIORITE_WALL);
        xMaterials.add(XMaterial.BLACKSTONE_WALL);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_WALL);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_BRICK_WALL);
        xMaterials.add(XMaterial.ANVIL);
        xMaterials.add(XMaterial.CHIPPED_ANVIL);
        xMaterials.add(XMaterial.DAMAGED_ANVIL);
        xMaterials.add(XMaterial.LIGHT_WEIGHTED_PRESSURE_PLATE);
        xMaterials.add(XMaterial.HEAVY_WEIGHTED_PRESSURE_PLATE);
        xMaterials.add(XMaterial.DAYLIGHT_DETECTOR);
        xMaterials.add(XMaterial.REDSTONE_BLOCK);
        xMaterials.add(XMaterial.NETHER_QUARTZ_ORE);
        xMaterials.add(XMaterial.CHISELED_QUARTZ_BLOCK);
        xMaterials.add(XMaterial.QUARTZ_BLOCK);
        xMaterials.add(XMaterial.QUARTZ_PILLAR);
        xMaterials.add(XMaterial.QUARTZ_STAIRS);
        xMaterials.add(XMaterial.DROPPER);
        xMaterials.add(XMaterial.TERRACOTTA);
        xMaterials.add(XMaterial.WHITE_TERRACOTTA);
        xMaterials.add(XMaterial.ORANGE_TERRACOTTA);
        xMaterials.add(XMaterial.MAGENTA_TERRACOTTA);
        xMaterials.add(XMaterial.LIGHT_BLUE_TERRACOTTA);
        xMaterials.add(XMaterial.YELLOW_TERRACOTTA);
        xMaterials.add(XMaterial.LIME_TERRACOTTA);
        xMaterials.add(XMaterial.PINK_TERRACOTTA);
        xMaterials.add(XMaterial.GRAY_TERRACOTTA);
        xMaterials.add(XMaterial.LIGHT_GRAY_TERRACOTTA);
        xMaterials.add(XMaterial.CYAN_TERRACOTTA);
        xMaterials.add(XMaterial.PURPLE_TERRACOTTA);
        xMaterials.add(XMaterial.BLUE_TERRACOTTA);
        xMaterials.add(XMaterial.BROWN_TERRACOTTA);
        xMaterials.add(XMaterial.GREEN_TERRACOTTA);
        xMaterials.add(XMaterial.RED_TERRACOTTA);
        xMaterials.add(XMaterial.BLACK_TERRACOTTA);
        xMaterials.add(XMaterial.IRON_DOOR);
        xMaterials.add(XMaterial.COAL_BLOCK);
        xMaterials.add(XMaterial.PACKED_ICE);
        xMaterials.add(XMaterial.WHITE_SHULKER_BOX);
        xMaterials.add(XMaterial.ORANGE_SHULKER_BOX);
        xMaterials.add(XMaterial.MAGENTA_SHULKER_BOX);
        xMaterials.add(XMaterial.LIGHT_BLUE_SHULKER_BOX);
        xMaterials.add(XMaterial.YELLOW_SHULKER_BOX);
        xMaterials.add(XMaterial.LIME_SHULKER_BOX);
        xMaterials.add(XMaterial.PINK_SHULKER_BOX);
        xMaterials.add(XMaterial.GRAY_SHULKER_BOX);
        xMaterials.add(XMaterial.LIGHT_GRAY_SHULKER_BOX);
        xMaterials.add(XMaterial.CYAN_SHULKER_BOX);
        xMaterials.add(XMaterial.PURPLE_SHULKER_BOX);
        xMaterials.add(XMaterial.BLUE_SHULKER_BOX);
        xMaterials.add(XMaterial.BROWN_SHULKER_BOX);
        xMaterials.add(XMaterial.GREEN_SHULKER_BOX);
        xMaterials.add(XMaterial.RED_SHULKER_BOX);
        xMaterials.add(XMaterial.BLACK_SHULKER_BOX);
        xMaterials.add(XMaterial.WHITE_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.ORANGE_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.MAGENTA_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.LIGHT_BLUE_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.YELLOW_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.LIME_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.PINK_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.GRAY_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.LIGHT_GRAY_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.CYAN_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.PURPLE_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.BLUE_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.BROWN_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.GREEN_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.RED_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.BLACK_STAINED_GLASS_PANE);
        xMaterials.add(XMaterial.PRISMARINE);
        xMaterials.add(XMaterial.PRISMARINE_BRICKS);
        xMaterials.add(XMaterial.DARK_PRISMARINE);
        xMaterials.add(XMaterial.PRISMARINE_STAIRS);
        xMaterials.add(XMaterial.PRISMARINE_BRICK_STAIRS);
        xMaterials.add(XMaterial.DARK_PRISMARINE_STAIRS);
        xMaterials.add(XMaterial.SEA_LANTERN);
        xMaterials.add(XMaterial.RED_SANDSTONE);
        xMaterials.add(XMaterial.CHISELED_RED_SANDSTONE);
        xMaterials.add(XMaterial.CUT_RED_SANDSTONE);
        xMaterials.add(XMaterial.RED_SANDSTONE_STAIRS);
        xMaterials.add(XMaterial.CUT_SANDSTONE);
        xMaterials.add(XMaterial.MAGMA_BLOCK);
        xMaterials.add(XMaterial.RED_NETHER_BRICKS);
        xMaterials.add(XMaterial.OBSERVER);
        xMaterials.add(XMaterial.SHULKER_BOX);
        xMaterials.add(XMaterial.WHITE_SHULKER_BOX);
        xMaterials.add(XMaterial.ORANGE_SHULKER_BOX);
        xMaterials.add(XMaterial.MAGENTA_SHULKER_BOX);
        xMaterials.add(XMaterial.LIGHT_BLUE_SHULKER_BOX);
        xMaterials.add(XMaterial.YELLOW_SHULKER_BOX);
        xMaterials.add(XMaterial.LIME_SHULKER_BOX);
        xMaterials.add(XMaterial.PINK_SHULKER_BOX);
        xMaterials.add(XMaterial.GRAY_SHULKER_BOX);
        xMaterials.add(XMaterial.LIGHT_GRAY_SHULKER_BOX);
        xMaterials.add(XMaterial.CYAN_SHULKER_BOX);
        xMaterials.add(XMaterial.PURPLE_SHULKER_BOX);
        xMaterials.add(XMaterial.BLUE_SHULKER_BOX);
        xMaterials.add(XMaterial.BROWN_SHULKER_BOX);
        xMaterials.add(XMaterial.GREEN_SHULKER_BOX);
        xMaterials.add(XMaterial.RED_SHULKER_BOX);
        xMaterials.add(XMaterial.BLACK_SHULKER_BOX);
        xMaterials.add(XMaterial.WHITE_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.ORANGE_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.MAGENTA_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.LIGHT_BLUE_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.YELLOW_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.LIME_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.PINK_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.GRAY_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.LIGHT_GRAY_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.CYAN_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.PURPLE_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.BLUE_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.BROWN_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.GREEN_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.RED_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.BLACK_GLAZED_TERRACOTTA);
        xMaterials.add(XMaterial.WHITE_CONCRETE);
        xMaterials.add(XMaterial.ORANGE_CONCRETE);
        xMaterials.add(XMaterial.MAGENTA_CONCRETE);
        xMaterials.add(XMaterial.LIGHT_BLUE_CONCRETE);
        xMaterials.add(XMaterial.YELLOW_CONCRETE);
        xMaterials.add(XMaterial.LIME_CONCRETE);
        xMaterials.add(XMaterial.PINK_CONCRETE);
        xMaterials.add(XMaterial.GRAY_CONCRETE);
        xMaterials.add(XMaterial.LIGHT_GRAY_CONCRETE);
        xMaterials.add(XMaterial.CYAN_CONCRETE);
        xMaterials.add(XMaterial.PURPLE_CONCRETE);
        xMaterials.add(XMaterial.BLUE_CONCRETE);
        xMaterials.add(XMaterial.BROWN_CONCRETE);
        xMaterials.add(XMaterial.GREEN_CONCRETE);
        xMaterials.add(XMaterial.RED_CONCRETE);
        xMaterials.add(XMaterial.BLACK_CONCRETE);
        xMaterials.add(XMaterial.DEAD_TUBE_CORAL_BLOCK);
        xMaterials.add(XMaterial.DEAD_BRAIN_CORAL_BLOCK);
        xMaterials.add(XMaterial.DEAD_BUBBLE_CORAL_BLOCK);
        xMaterials.add(XMaterial.DEAD_FIRE_CORAL_BLOCK);
        xMaterials.add(XMaterial.DEAD_HORN_CORAL_BLOCK);
        xMaterials.add(XMaterial.BLUE_ICE);
        xMaterials.add(XMaterial.POLISHED_GRANITE_STAIRS);
        xMaterials.add(XMaterial.SMOOTH_RED_SANDSTONE_STAIRS);
        xMaterials.add(XMaterial.MOSSY_STONE_BRICK_STAIRS);
        xMaterials.add(XMaterial.POLISHED_DIORITE_STAIRS);
        xMaterials.add(XMaterial.MOSSY_COBBLESTONE_STAIRS);
        xMaterials.add(XMaterial.END_STONE_BRICK_STAIRS);
        xMaterials.add(XMaterial.STONE_STAIRS);
        xMaterials.add(XMaterial.SMOOTH_SANDSTONE_STAIRS);
        xMaterials.add(XMaterial.SMOOTH_QUARTZ_STAIRS);
        xMaterials.add(XMaterial.GRANITE_STAIRS);
        xMaterials.add(XMaterial.ANDESITE_STAIRS);
        xMaterials.add(XMaterial.POLISHED_ANDESITE_STAIRS);
        xMaterials.add(XMaterial.DIORITE_STAIRS);
        xMaterials.add(XMaterial.POLISHED_GRANITE_SLAB);
        xMaterials.add(XMaterial.SMOOTH_RED_SANDSTONE_SLAB);
        xMaterials.add(XMaterial.MOSSY_STONE_BRICK_SLAB);
        xMaterials.add(XMaterial.POLISHED_DIORITE_SLAB);
        xMaterials.add(XMaterial.MOSSY_COBBLESTONE_SLAB);
        xMaterials.add(XMaterial.END_STONE_BRICK_SLAB);
        xMaterials.add(XMaterial.SMOOTH_SANDSTONE_SLAB);
        xMaterials.add(XMaterial.SMOOTH_QUARTZ_SLAB);
        xMaterials.add(XMaterial.GRANITE_SLAB);
        xMaterials.add(XMaterial.ANDESITE_SLAB);
        xMaterials.add(XMaterial.RED_NETHER_BRICK_SLAB);
        xMaterials.add(XMaterial.POLISHED_ANDESITE_SLAB);
        xMaterials.add(XMaterial.DIORITE_SLAB);
        xMaterials.add(XMaterial.IRON_DOOR);
        xMaterials.add(XMaterial.CAULDRON);
        xMaterials.add(XMaterial.SMOKER);
        xMaterials.add(XMaterial.BLAST_FURNACE);
        xMaterials.add(XMaterial.GRINDSTONE);
        xMaterials.add(XMaterial.SMITHING_TABLE);
        xMaterials.add(XMaterial.STONECUTTER);
        xMaterials.add(XMaterial.LODESTONE);
        xMaterials.add(XMaterial.NETHERITE_BLOCK);
        xMaterials.add(XMaterial.ANCIENT_DEBRIS);
        xMaterials.add(XMaterial.CRYING_OBSIDIAN);
        xMaterials.add(XMaterial.BLACKSTONE);
        xMaterials.add(XMaterial.BLACKSTONE_SLAB);
        xMaterials.add(XMaterial.BLACKSTONE_STAIRS);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_SLAB);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_STAIRS);
        xMaterials.add(XMaterial.CHISELED_POLISHED_BLACKSTONE);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_BRICKS);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_BRICK_SLAB);
        xMaterials.add(XMaterial.POLISHED_BLACKSTONE_BRICK_STAIRS);
        xMaterials.add(XMaterial.CRACKED_POLISHED_BLACKSTONE_BRICKS);
        xMaterials.add(XMaterial.RESPAWN_ANCHOR);

        Set<Material> clinkMaterials = new HashSet<>();

        for (XMaterial xMaterial : xMaterials) {
            if (xMaterial.isSupported()) clinkMaterials.add(xMaterial.parseMaterial());
        }

        return clinkMaterials;
    }

    public static boolean isClinkMaterial(Material material) {
        return clinkMaterial.contains(material);
    }


}
