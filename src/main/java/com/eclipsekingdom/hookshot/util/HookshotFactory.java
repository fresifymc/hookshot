package com.eclipsekingdom.hookshot.util;

import com.eclipsekingdom.hookshot.sys.Language;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class HookshotFactory {

    public static boolean isHookshot(ItemStack itemStack) {
        return itemStack != null &&
                itemStack.getType() == Material.FISHING_ROD &&
                itemStack.hasItemMeta() &&
                itemStack.getItemMeta().hasCustomModelData() &&
                (itemStack.getItemMeta().getCustomModelData() == 300 || itemStack.getItemMeta().getCustomModelData() == 301);
    }

    private static ItemStack hookshotItem = buildHookShot();

    private static ItemStack buildHookShot() {
        ItemStack itemStack = new ItemStack(Material.FISHING_ROD);
        ItemMeta meta = itemStack.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Hookshot");
        List<String> lore = new ArrayList<>();
        String[] toAdd = Language.ITEM_LORE.toString().split("\\\\n");
        if (toAdd.length > 0) {
            for (String s : toAdd) {
                lore.add(s);
            }
            meta.setLore(lore);
        }
        meta.setCustomModelData(300);
        itemStack.setItemMeta(meta);
        return itemStack;
    }

    public static ItemStack getHookshotItem() {
        return hookshotItem.clone();
    }

}
