package com.eclipsekingdom.hookshot.util;

import com.eclipsekingdom.hookshot.Hookshot;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Stun extends BukkitRunnable {

    private static Map<UUID, Stun> stunnedEntities = new HashMap<>();

    public static boolean isStunned(Entity entity) {
        return stunnedEntities.containsKey(entity.getUniqueId());
    }

    public static Stun getStun(Entity entity) {
        return stunnedEntities.get(entity.getUniqueId());
    }

    private Player player;
    private LivingEntity entity;
    private Vector dir;
    private UUID pid;

    public Stun(LivingEntity entity, UUID pid, Player player) {
        this.player = player;
        this.pid = pid;
        UUID entityID = entity.getUniqueId();
        if (stunnedEntities.containsKey(entityID)) stunnedEntities.get(entityID).end();
        stunnedEntities.put(entityID, this);
        this.entity = entity;
        this.dir = entity.getLocation().getDirection();
        runTaskTimer(Hookshot.getPlugin(), 0, 1);
        if(entity instanceof Monster) ((Monster) entity).setTarget(null);
    }

    @Override
    public void run() {
        Location location = entity.getLocation();
        location.setDirection(dir);
        entity.teleport(location);
    }

    public void end() {
        if(entity instanceof Monster) ((Monster) entity).setTarget(player);
        stunnedEntities.remove(entity.getUniqueId());
        cancel();
    }

    public UUID getPid() {
        return pid;
    }
}
