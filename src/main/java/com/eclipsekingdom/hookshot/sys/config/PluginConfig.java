package com.eclipsekingdom.hookshot.sys.config;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public class PluginConfig {

    private static File file = new File("plugins/Hookshot", "config.yml");
    private static FileConfiguration config = YamlConfiguration.loadConfiguration(file);

    private static String languageFileString = "Language file";
    private static String languageFile = "en";

    public PluginConfig() {
        load();
    }

    private void load() {
        if (file.exists()) {
            try {
                languageFile = config.getString(languageFileString, languageFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getLanguageFile() {
        return languageFile;
    }

}
