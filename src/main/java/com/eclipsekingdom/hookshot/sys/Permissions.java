package com.eclipsekingdom.hookshot.sys;

import org.bukkit.command.CommandSender;

public class Permissions {

    private static String HOOK_GIVE = "hookshot.give";

    public static boolean canGive(CommandSender sender) {
        return hasPermission(sender, HOOK_GIVE);
    }

    private static boolean hasPermission(CommandSender sender, String permString) {
        return (sender.hasPermission("hookshot.*") || sender.hasPermission(permString));
    }

}
