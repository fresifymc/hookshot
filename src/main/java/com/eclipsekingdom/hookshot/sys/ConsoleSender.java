package com.eclipsekingdom.hookshot.sys;

import org.bukkit.Bukkit;

public class ConsoleSender {

    public static void sendMessage(String message) {
        Bukkit.getConsoleSender().sendMessage("[Hookshot] " + message);
    }

}
