package com.eclipsekingdom.hookshot.sys;

import com.eclipsekingdom.hookshot.sys.config.PluginConfig;
import com.eclipsekingdom.hookshot.util.HookUtil;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Language {

    CONSOLE_FILE_ERROR("Console - file error", "Error saving %file%"),

    ARG_AMOUNT("Arg - amount", "amount"),
    ARG_PLAYER("Arg - player", "player"),

    WARN_NOT_PERMITTED("Warn - no permission", "You do not have permission for this command."),
    WARN_PLAYER_OFFLINE("Warn - player offline", "%player% is not online."),

    MISC_FORMAT("Misc - format", "Format is %format%"),

    SUCCESS_ITEMS_SENT("Success - items sent", "items sent to %player%"),

    ITEM_NAME("Item - name", "Hookshot"),
    ITEM_LORE("Item - lore", "&9a treasure that stretches--BOING! \\n&9and shrinks--BOING!"),

    ;

    private static File file = new File("plugins/Hookshot/Locale", PluginConfig.getLanguageFile() + ".yml");
    private static FileConfiguration config = YamlConfiguration.loadConfiguration(file);

    public static void load() {
        if (file.exists()) {
            try {
                for (Language message : Language.values()) {
                    Language.MessageSetting setting = message.getMessageSetting();
                    if (config.contains(setting.getLabel())) {
                        setting.setMessage(config.getString(setting.getLabel(), setting.getMessage()));
                    } else {
                        config.set(setting.getLabel(), setting.getMessage());
                    }
                }
                HookUtil.save(config, file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class MessageSetting {

        private String label;
        private String message;

        public MessageSetting(String label, String message) {
            this.label = label;
            this.message = message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getLabel() {
            return label;
        }

        public String getMessage() {
            return message;
        }

    }

    private MessageSetting messageSetting;

    Language(String messageSetting, String messageDefault) {
        this.messageSetting = new MessageSetting(messageSetting, messageDefault);
    }

    public MessageSetting getMessageSetting() {
        return messageSetting;
    }

    @Override
    public String toString() {
        return get();
    }

    private String get() {
        return ChatColor.translateAlternateColorCodes('&', messageSetting.getMessage());
    }

    public String fromFile(String fileName) {
        return get().replaceAll("%file%", fileName);
    }

    public String fromFormat(String format) {
        return get().replaceAll("%format%", format);
    }

    public String fromPlayer(String playerName) {
        return get().replaceAll("%player%", playerName);
    }


}
