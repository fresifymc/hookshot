package com.eclipsekingdom.hookshot;

import com.eclipsekingdom.hookshot.util.HookUtil;
import com.eclipsekingdom.hookshot.util.Stun;
import com.eclipsekingdom.hookshot.version.VUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.type.Piston;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.*;

public class HookAction extends BukkitRunnable {

    private static Map<UUID, HookAction> playerToAction = new HashMap<>();
    private static Map<UUID, HookAction> hookToAction = new HashMap<>();
    private static Map<UUID, HookAction> hookedEntityToAction = new HashMap<>();
    private static Map<Location, HookAction> blockLocToAction = new HashMap<>();

    public static boolean isHookingPlayer(Entity entity) {
        return playerToAction.containsKey(entity.getUniqueId());
    }

    public static boolean isHook(Entity entity) {
        return hookToAction.containsKey(entity.getUniqueId());
    }

    public static boolean isHookedEntity(Entity entity) {
        return hookedEntityToAction.containsKey(entity.getUniqueId());
    }

    public static boolean isHookedBlockLoc(Location location) {
        return blockLocToAction.containsKey(location);
    }

    public static HookAction getActionFromHook(Entity hook) {
        return hookToAction.get(hook.getUniqueId());
    }

    public static HookAction getActionFromPlayer(Player player) {
        return playerToAction.get(player.getUniqueId());
    }

    public static HookAction getActionFromHookedEntity(Entity entity) {
        return hookedEntityToAction.get(entity.getUniqueId());
    }

    public static HookAction getActionFromBlockLoc(Location location) {
        return blockLocToAction.get(location);
    }

    private static final PotionEffect INVISIBLE = new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 0, false, false);
    private static final double SPEED = 1.7;
    private static final double TOTAL_CHAINS = 27;
    private static final Vector STANDING = new Vector(0, 0, 0);
    private static Random random = new Random();


    private UUID pid = UUID.randomUUID();
    private Player player;
    private Entity hookedEntity;
    private Location hookedPosition;
    private Location hookedBlockPosition;
    private ItemStack itemStack;
    private Arrow hook;
    private Vector dir;
    private Bat bat;
    private int chainsLaunched = 0;
    private State state = State.LAUNCHING;
    private World world;
    private int ticks = 0;

    public HookAction(Player player, ItemStack itemStack) {
        this.player = player;
        this.world = player.getWorld();
        playerToAction.put(player.getUniqueId(), this);
        this.itemStack = itemStack;
        HookUtil.setModel(itemStack, 301);

        this.dir = player.getLocation().getDirection();

        Location loc = player.getEyeLocation().add(dir.clone().multiply(0.5));
        spawnHook(loc);

        bat = VUtil.spawnBat(player);
        bat.addPotionEffect(INVISIBLE);
        bat.setInvulnerable(true);
        bat.setAI(false);
        bat.setSilent(true);
        bat.setGravity(false);
        bat.setLeashHolder(player);

        runTaskTimer(Hookshot.getPlugin(), 0, 1);

        if (!hook.getLocation().getBlock().isPassable()) end();
    }

    private void spawnHook(Location loc) {
        hook = (Arrow) world.spawnEntity(loc, EntityType.ARROW);
        hook.setGravity(false);
        hook.setSilent(true);
        hook.setShooter(player);
        hookToAction.put(hook.getUniqueId(), this);
    }

    @Override
    public void run() {
        ticks++;
        if (state == State.LAUNCHING) {
            if (chainsLaunched == TOTAL_CHAINS) {
                state = State.FAST_RETRACT;
            } else {
                hook.setVelocity(dir.clone().multiply(SPEED));
                bat.teleport(hook.getLocation());
                playSound();
                doItemCheck();
                chainsLaunched++;
            }
        } else {
            playSound();
            if (state == State.FAST_RETRACT) {
                if (player.getEyeLocation().subtract(0, 0.2, 0).distance(hook.getLocation()) > 3.5 && chainsLaunched > -20) {
                    dir = player.getEyeLocation().subtract(0, 0.2, 0).subtract(hook.getLocation()).toVector().normalize();
                    hook.setVelocity(dir.clone().multiply(SPEED * 2));
                    bat.teleport(hook.getLocation().add(0, -0.5, 0));
                    chainsLaunched -= 2;
                } else {
                    end();
                }
            } else if (state == State.PLAYER_PULL_BLOCK) {
                Location playerUpper = player.getEyeLocation().subtract(0, 0.25, 0);
                Location playerLower = player.getLocation().add(0, 0.25, 0);
                if (!(playerUpper.distance(hookedPosition) <= 1.5 || playerLower.distance(hookedPosition) <= 1.5 || chainsLaunched <= -20)) {
                    Location playerGripPos = (ticks / 10) % 2 == 0 ? playerUpper : playerLower;
                    dir = hook.getLocation().subtract(playerGripPos).toVector().normalize();
                    if (ticks % 2 == 0) player.setVelocity(dir.clone().multiply(SPEED));
                    chainsLaunched -= 1;
                } else {
                    end();
                }
            } else if (state == State.PLAYER_PULL_MOB) {
                if (!hookedEntity.isDead()) {
                    hookedPosition = hookedEntity.getLocation();
                    Location playerUpper = player.getEyeLocation().subtract(0, 0.25, 0);
                    Location playerLower = player.getLocation().add(0, 0.25, 0);
                    if (!(playerUpper.distance(hookedPosition) <= 3 || playerLower.distance(hookedPosition) <= 3 || chainsLaunched <= -20)) {
                        Location playerGripPos = (ticks / 10) % 2 == 0 ? playerUpper : playerLower;
                        dir = hook.getLocation().subtract(playerGripPos).toVector().normalize();
                        if (ticks % 2 == 0) player.setVelocity(dir.clone().multiply(SPEED));
                        chainsLaunched -= 1;
                    } else {
                        end();
                    }
                } else {
                    cancelAction();
                }
            } else if (state == State.MOB_PULL) {
                if (player.getLocation().add(0, 0.3, 0).distance(hookedEntity.getLocation()) > 1.5 && chainsLaunched > -20) {
                    dir = player.getLocation().add(0, 0.3, 0).subtract(hookedEntity.getLocation()).toVector().normalize();
                    hookedEntity.setVelocity(dir.clone().multiply(SPEED));
                    chainsLaunched -= 1;
                } else {
                    end();
                }
            } else if (state == State.ITEM_PULL) {
                if (player.getEyeLocation().distance(hook.getLocation()) > 0.45 && chainsLaunched > -20) {
                    dir = player.getEyeLocation().subtract(hook.getLocation()).toVector().normalize();
                    hook.setVelocity(dir.clone().multiply(SPEED));
                    hookedEntity.setVelocity(dir.clone().multiply(SPEED));
                    bat.teleport(hook.getLocation().add(0, -0.5, 0));
                    chainsLaunched -= 1;
                } else {
                    end();
                }
            }
        }
    }

    private void doItemCheck() {
        List<Entity> closeItems = new ArrayList<>();
        for (Entity item : hook.getNearbyEntities(0.44, 0.64, 0.44)) {
            if (item instanceof Item) {
                closeItems.add(item);
            }
        }
        if (!closeItems.isEmpty()) {
            hookedEntity = closeItems.get(0);
            state = State.ITEM_PULL;
        }
    }

    private void playSound() {
        if (ticks % 2 == 0)
            world.playSound(player.getLocation(), Sound.BLOCK_IRON_TRAPDOOR_OPEN, 0.5f, (float) (1.1 + 0.3 * random.nextDouble()));
    }

    public void onHitEntity(Entity entity) {
        if (entity instanceof LivingEntity || entity.getType() == EntityType.UNKNOWN) {
            if (!entity.getUniqueId().equals(player.getUniqueId())) {
                if (!HookUtil.hasProtection(entity)) {
                    hookedEntity = entity;
                    hookedEntityToAction.put(hookedEntity.getUniqueId(), this);
                    if (!HookUtil.isHeavy(entity)) {
                        popOffIfRiding(entity);
                        state = State.MOB_PULL;
                        if (!(entity instanceof Player) && entity instanceof LivingEntity) {
                            new Stun((LivingEntity) entity, pid, player);
                        }
                    } else {
                        popOffIfRiding(player);
                        state = State.PLAYER_PULL_MOB;
                    }
                } else {
                    Location hookLoc = hook.getLocation();
                    hookToAction.remove(hook.getUniqueId());
                    hook.remove();
                    Vector towardsPlayer = player.getLocation().subtract(hookLoc).toVector().normalize();
                    hookLoc.setDirection(towardsPlayer);
                    hookLoc.add(towardsPlayer.multiply(0.5));
                    spawnHook(hookLoc);
                    world.playSound(entity.getLocation(), Sound.BLOCK_ANVIL_PLACE, 0.5f, (float) (1.1 + 0.2 * random.nextDouble()));
                    state = State.FAST_RETRACT;
                }
            } else {
                end();
            }
        } else {
            if (state != State.FAST_RETRACT) {
                state = State.FAST_RETRACT;
            } else {
                end();
            }
        }
    }

    private void popOffIfRiding(Entity rider) {
        Entity stead = getStead(rider);
        if (stead != null) stead.removePassenger(rider);
    }

    private Entity getStead(Entity rider) {
        for (Entity entity : rider.getNearbyEntities(1, 1, 1)) {
            if (entity.getPassengers().contains(rider)) {
                return entity;
            }
        }
        return null;
    }

    public void onHitBlock(Block block, BlockFace blockFace) {
        if (HookUtil.isGrappleMaterial(block.getType()) || isPistonTop(block, blockFace)) {
            hookedBlockPosition = block.getLocation();
            blockLocToAction.put(hookedBlockPosition, this);
            popOffIfRiding(player);
            state = State.PLAYER_PULL_BLOCK;
            hookedPosition = block.getLocation().add(0.5, 0.5, 0.5);
            world.playSound(hookedPosition, Sound.ENTITY_ARROW_HIT, 0.5f, 0.88f);
        } else {
            if (state != State.FAST_RETRACT) {

                //subtract 1 from hook location because it registers as above block that it hit
                hook.teleport(hook.getLocation().add(0, -1, 0));
                doItemCheck();

                Location hookLoc = hook.getLocation().add(0, 1, 0);
                hookToAction.remove(hook.getUniqueId());
                hook.remove();
                Vector towardsPlayer = player.getLocation().subtract(hookLoc).toVector().normalize();
                hookLoc.setDirection(towardsPlayer);
                hookLoc.add(towardsPlayer.multiply(0.5));

                spawnHook(hookLoc);

                if (state != State.ITEM_PULL) {
                    if (HookUtil.isClinkMaterial(block.getType())) {
                        world.playSound(hookLoc, Sound.BLOCK_ANVIL_PLACE, 0.5f, (float) (1.1 + 0.2 * random.nextDouble()));
                    } else {
                        world.playSound(hookLoc, Sound.ENTITY_GENERIC_BIG_FALL, 0.5f, (float) (0.6 + 0.2 * random.nextDouble()));
                    }
                    state = State.FAST_RETRACT;
                }
            } else {
                end();
            }
        }
    }

    private boolean isPistonTop(Block block, BlockFace blockFace) {
        if (block.getBlockData() instanceof Piston) {
            return ((Piston) block.getBlockData()).getFacing().equals(blockFace);
        } else {
            return false;
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void end() {
        if (state != State.ENDING) {
            State prevState = state;
            state = State.ENDING;
            if (prevState == State.PLAYER_PULL_BLOCK || prevState == State.PLAYER_PULL_MOB)
                player.setVelocity(STANDING);
            if (prevState == State.MOB_PULL) hookedEntity.setVelocity(STANDING);
            if (hookedEntity != null) {
                hookedEntityToAction.remove(hookedEntity.getUniqueId());
                if (Stun.isStunned(hookedEntity)) {
                    Stun stun = Stun.getStun(hookedEntity);
                    if (stun.getPid().equals(pid)) stun.end();
                }
            }
            UUID hookID = hook.getUniqueId();
            hook.remove();
            bat.setLeashHolder(null);
            bat.remove();
            cancel();

            //Delaying one tick prevents player from being damaged by arrow on return
            Bukkit.getScheduler().runTaskLater(Hookshot.getPlugin(), () -> {
                blockLocToAction.remove(hookedBlockPosition);
                hookToAction.remove(hookID);
                playerToAction.remove(player.getUniqueId());
                ItemMeta meta = itemStack.getItemMeta();
                meta.setCustomModelData(300);
                itemStack.setItemMeta(meta);
            }, 1);
        }
    }

    public void cancelAction() {
        if (state == State.PLAYER_PULL_BLOCK) player.setVelocity(player.getVelocity().multiply(0.2));
        if (state == State.MOB_PULL) hookedEntity.setVelocity(hookedEntity.getVelocity().multiply(0.2));
        state = State.CANCELLED;
        end();
    }

    public void setState(State state) {
        this.state = state;
    }

    public enum State {
        LAUNCHING,
        ITEM_PULL,
        MOB_PULL,
        PLAYER_PULL_BLOCK,
        PLAYER_PULL_MOB,
        FAST_RETRACT,
        CANCELLED,
        ENDING,
    }

}
