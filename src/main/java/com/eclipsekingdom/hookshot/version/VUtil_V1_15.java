package com.eclipsekingdom.hookshot.version;

import com.eclipsekingdom.hookshot.Hookshot;
import com.google.common.collect.ImmutableSet;
import net.minecraft.server.v1_15_R1.EntityBat;
import net.minecraft.server.v1_15_R1.EntityTypes;
import net.minecraft.server.v1_15_R1.World;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_15_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.util.Set;

public class VUtil_V1_15 implements IVUtil {

    @Override
    public Bat spawnBat(Player player) {
        Location spawnLoc = player.getLocation().add(0, 0.5, 0);
        World world = ((CraftWorld) player.getWorld()).getHandle().getMinecraftWorld();
        EntityBat batEntity = new EntityBat(EntityTypes.BAT, world);
        batEntity.setLocation(spawnLoc.getX(), spawnLoc.getY(), spawnLoc.getZ(), 0, 0);
        batEntity.setLeashHolder(((CraftPlayer) player).getHandle(), false);
        batEntity.setInvisible(true);
        world.addEntity(batEntity, CreatureSpawnEvent.SpawnReason.CUSTOM);
        return (Bat) batEntity.getBukkitEntity();
    }

    @Override
    public ShapedRecipe getShapedRecipe(String name, ItemStack itemStack) {
        return new ShapedRecipe(new NamespacedKey(Hookshot.getPlugin(), name), itemStack);
    }

    @Override
    public Set<EntityType> getBigEntities() {
        return new ImmutableSet.Builder<EntityType>()

                .add(EntityType.SLIME)
                .add(EntityType.PHANTOM)

                .add(EntityType.PANDA)
                .add(EntityType.POLAR_BEAR)

                .add(EntityType.SKELETON_HORSE)
                .add(EntityType.ZOMBIE_HORSE)
                .add(EntityType.DONKEY)
                .add(EntityType.HORSE)
                .add(EntityType.LLAMA)

                .add(EntityType.GHAST)
                .add(EntityType.ELDER_GUARDIAN)

                .add(EntityType.IRON_GOLEM)
                .add(EntityType.RAVAGER)
                .add(EntityType.WITHER)
                .add(EntityType.GIANT)
                .add(EntityType.ENDER_DRAGON)
                .add(EntityType.UNKNOWN)
                .add(EntityType.SHULKER)
                .build();
    }

}
