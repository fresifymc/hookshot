package com.eclipsekingdom.hookshot.version;

import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.util.Set;

public interface IVUtil {

    Bat spawnBat(Player player);

    ShapedRecipe getShapedRecipe(String name, ItemStack itemStack);

    Set<EntityType> getBigEntities();

}
