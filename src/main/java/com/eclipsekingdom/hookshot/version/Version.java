package com.eclipsekingdom.hookshot.version;

import org.bukkit.Bukkit;

public enum Version {

    V1_16(116),
    V1_15(115),
    V1_14(114),
    UNKNOWN(0),
    ;

    public static Version current = getVersion();

    private static Version getVersion() {
        String versionString = Bukkit.getVersion();
        if (versionString.contains("1.16")) {
            return V1_16;
        } else if (versionString.contains("1.15")) {
            return V1_15;
        } else if (versionString.contains("1.14")) {
            return V1_14;
        } else {
            return UNKNOWN;
        }
    }

    public final int value;

    Version(int value) {
        this.value = value;
    }

    public boolean hasOffhand() {
        return value >= 109;
    }


}
