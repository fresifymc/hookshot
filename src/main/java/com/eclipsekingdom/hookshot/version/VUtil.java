package com.eclipsekingdom.hookshot.version;

import org.bukkit.entity.Bat;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;

import java.util.Set;

public class VUtil {

    private static IVUtil iVUtil = selectIVUtil();

    private static IVUtil selectIVUtil() {
        switch (Version.current) {
            case V1_16:
                return new VUtil_V1_16();
            case V1_15:
                return new VUtil_V1_15();
            case V1_14:
                return new VUtil_V1_14();
            default:
                return null;
        }
    }

    public static Bat spawnBat(Player player) {
        return iVUtil.spawnBat(player);
    }

    public static ShapedRecipe getShapedRecipe(String name, ItemStack itemStack) {
        return iVUtil.getShapedRecipe(name, itemStack);
    }

    public static Set<EntityType> getBigEntities() {
        return iVUtil.getBigEntities();
    }

}
