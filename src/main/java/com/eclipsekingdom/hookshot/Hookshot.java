package com.eclipsekingdom.hookshot;

import com.eclipsekingdom.hookshot.sys.Language;
import com.eclipsekingdom.hookshot.sys.config.ConfigLoader;
import com.eclipsekingdom.hookshot.sys.config.PluginConfig;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class Hookshot extends JavaPlugin {

    private static Plugin plugin;

    @Override
    public void onEnable() {
        plugin = this;

        ConfigLoader.load();
        new PluginConfig();
        Language.load();

        getCommand("hookshot").setExecutor(new CommandHookshot());

        new HookListener();
    }

    @Override
    public void onDisable() {

    }

    public static Plugin getPlugin() {
        return plugin;
    }
}
